namespace ProAgil.API.Dtos
{
    public class UserDto
    {
        // através do UserDto digo os campos de User q quero retornar para o usuário
       
        public string UserName {get; set;}
        public string Email {get; set;}
        public string Password {get; set;}
        public string FullName {get; set;}
    }
}