using System.ComponentModel.DataAnnotations;

namespace ProAgil.API.Dtos
{
    public class RedeSocialDto
    {
        /*
            não precisamos das informações comentadas para retornar algo sobre a rede social para 
            quem está consumindo nossa API

            pois, estando em rede social eu n preciso saber do evento e do palestrante, agr estando
            em evento preciso saber qual a rede social q está armazenando sua publicação, assim como
            estando em palestrante preciso saber qual a sua rede social

            eh necessário o Id da rede social, pq sem ele qnd for fazer um PUT, os dados q estiverem
            na chave da rede social serão inseridos no bd e não atualizados como deveriam
        */
    
        public int Id {get; set;}        

        [Required(ErrorMessage="O campo {0} é obrigatório")] // {0} => será o atributo Nome
        public string Nome {get; set;}

        [Required]
        public string Url {get; set;}

        // public int? EventoId {get; set;}
        // public int? PalestranteId {get; set;}

        // public Evento Evento {get;}
        // public Palestrante Palestrante {get;}
    }
}