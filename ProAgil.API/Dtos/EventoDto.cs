using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ProAgil.API.Dtos
{
    /*
        existem certas situações q n eh preciso retornar td de um evento, usuário, por exemplo,
        basta alguns dados, eh por isso q iremos trabalhar com mapeamento, DTO (DATA TRANSFER OBJECT),
        ou seja, um objeto irá transferir os dados q selecionamos para a interface, para quem está
        consumindo minha API e n para o bd
    */

    public class EventoDto
    {
        public int Id {get; set;}

        [Required(ErrorMessage="Campo Obrigatório")]
        [StringLength(100, MinimumLength=3, ErrorMessage="Local é entre 3 e 100 caracteres")]
        public string Local {get; set;}

        public string DataEvento {get; set;}

        [Required(ErrorMessage="Tema é obrigatório")]
        public string Tema {get; set;}
        
        [Range(2, 120000, ErrorMessage="Quantidade de pessoas é entre 2 e 120000")]
        public int QtdPessoas {get; set;}
        public string UrlImagem {get; set;}

        [Phone]
        public string Telefone {get; set;}

        [EmailAddress]
        public string Email {get; set;}

        public List<LoteDto> Lotes {get; set;}

        public List<RedeSocialDto> RedesSociais {get; set;}

        public List<PalestranteDto> Palestrantes {get; set;}
    }
}