using System.ComponentModel.DataAnnotations;

namespace ProAgil.API.Dtos
{
    public class LoteDto
    {
        public int Id {get; set;}

        [Required]
        public string Nome {get; set;}

        [Required]
        public decimal Preco {get; set;}

        // como vai ser algo retornado para a interface e n para o bd, usaremos string
        public string DataInicio {get; set;}
        public string DataFim {get; set;}
        // public DateTime? DataInicio {get; set;}
        // public DateTime? DataFim {get; set;}

        [Range(2, 120000)]
        public int Quantidade {get; set;}

        // n precisamos saber o id do evento
        // public int EventoId {get; set;}
    }
}