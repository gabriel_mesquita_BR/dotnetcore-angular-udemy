﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
//using ProAgil.API.Data;
//using ProAgil.API.Models;
using ProAgil.Domain;
using ProAgil.Repository;

namespace ProAgil.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ValuesController : ControllerBase
    {
        private readonly ProAgilContext _context;
        public ValuesController(ProAgilContext context)
        {
            this._context = context; //armazena a conexão com o bd
        }

        // GET api/values
        // [HttpGet]
        // public ActionResult<IEnumerable<Evento>> Get()
        // {
        //     return this._context.Eventos.ToList();
        // }

        
        /*
            ActionResult está mt acoplado ao Razor, retorno de views, por isso vamos usar o
            IActionResult
        */

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            //seu retorno deve ser utilizando o método Ok

            try 
            {
                var results = await this._context.Eventos.ToListAsync();
                return Ok(results);

            }catch(Exception exception)
            {
                //throw new Exception(exception.Message);

                // Response.StatusCode = 404;
                // return new ObjectResult(new {msg = "Evento não encontrado!"});

                return this.StatusCode(StatusCodes.Status500InternalServerError, "Banco de dados falhou");
            }
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Evento>> Get(int id)
        {

            try 
            {
                var results = await this._context.Eventos.FirstOrDefaultAsync(x => x.Id == id);
                return Ok(results);

            }catch(Exception exception)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, "Banco de dados falhou");
            }

            // return new Evento[] {

            //     new Evento()
            //     {
            //         EventoId   = 1,
            //         Tema       = "Angular e .Net Core",
            //         Local      = "Belo Horizonte",
            //         Lote       = "1º Lote",
            //         QtdPessoas = 250,
            //         DataEvento = DateTime.Now.AddDays(2).ToString("dd/MM/yyyy")
            //     },

            //     new Evento()
            //     {
            //         EventoId   = 2,
            //         Tema       = "Angular e suas novidades",
            //         Local      = "Rio de janeiro",
            //         Lote       = "2º Lote",
            //         QtdPessoas = 450,
            //         DataEvento = DateTime.Now.AddDays(3).ToString("dd/MM/yyyy")
            //     }

            // }.FirstOrDefault(x => x.EventoId == id); //ao colocar esse comando, só retorna 1 Evento
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
