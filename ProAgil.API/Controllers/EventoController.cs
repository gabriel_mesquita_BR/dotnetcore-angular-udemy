using System.Net.Http.Headers;
using System.Net.Cache;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ProAgil.API.Dtos;
using ProAgil.Domain;
using ProAgil.Repository;
using System.IO;
using System.Linq;

namespace ProAgil.API.Controllers
{
    [Route("api/[controller]")]

    /*
        caso eu n passe essa anotação ApiController, eh necessário eu passar a anotação
        [FromBody] no método post, por exemplo, para especificar q eu tô querendo pegar os dados
        do corpo da requisição, pq senão colocar ele vai tentar pegar por query string, ou seja,
        da url

        outra desvantagem eh q a validação por data annotation n servirá se n passarmos no post ou
        put, por exemplo, o seguinte comando:

        se o estado do model n for válido, ou seja, tiver campos n preenchidos corretamente ou q n
        foram preenchidos

        if(!ModelState.IsValid)
            return this.StatusCode(StatusCodes.Status400BadRequest, ModelState);
    */

    [ApiController]
    public class EventoController : ControllerBase
    {
        private readonly IProAgilRepository _repo;
        private readonly IMapper _mapper;

        /*
            injeto como dependência a interface IProAgilRepository, logo como foi definido na 
            classe Startup no comando services.AddScoped, injeto a classe ProAgilRepository

            injeto auto mapper como dependência, lembrando q só eh possível isso, pois na classe
            Startup coloquei services.AddAutoMapper(typeof<Startup>)
        */
        public EventoController(IProAgilRepository repo, IMapper mapper)
        {
            this._repo   = repo;
            this._mapper = mapper;
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            try 
            {
                var eventos = await this._repo.GetAllEventosAsync(true);

                /*  eventos armazena todos os dados do bd
                    usando mapper eh possível delimitar (selecionar) somentes os dados relevantes
                    a serem exibidos na interface

                    como eventos armazena uma lista, eh necessário q EventoDto seja uma lista, por isso
                    o IEnumerable

                    uma mudança nítida logo de cara eh q o objeto retornado n mostra mais uma chave
                    chamada palestranteseventos, e sim, palestrantes, isso o objeto sendo do tipo
                    Evento, claro, pois se o objeto fosse do tipo Palestrante, a chave correta 
                    seria eventos

                    OBS: abaixo temos um mapeamento de domínio para dto
                */
                var results = this._mapper.Map<IEnumerable<EventoDto>>(eventos);

                return Ok(results); // retorno os dados filtrados

            }catch(Exception)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, "Banco de dados falhou");
            }
        }

        [HttpPost("upload")]
        public IActionResult Upload()
        {
            try 
            {
                var file       = Request.Form.Files[0]; // arquivo sempre retorna no formato array
                
                // combino os diretórios q as imagens serão salvas
                var folderName = Path.Combine("Resources", "Images");

                // combino o caminho q as imagens serão salvas partindo do diretório atual (q eh este)
                var pathToSave = Path.Combine(Directory.GetCurrentDirectory(), folderName);

                if(file.Length > 0)
                {
                    // pego o nome do arquivo
                    var fileName = ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName;
                    
                    // combino o caminho q as imagens serão salvas com o nome do arquivo
                    // se o nome do arquivo tiver aspas duplas substituo por string vazio
                    // e se tiver espaços removo através do Trim()    
                    var fullPath = Path.Combine(pathToSave, fileName.Replace("\"", "").Trim());

                    // crio a imagem no caminho definido
                    using(var stream = new FileStream(fullPath, FileMode.Create))
                        file.CopyTo(stream);
                }

                return Ok();

            }catch(Exception)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, "Banco de dados falhou");
            }

            // return BadRequest("Erro ao tentar realizar o upload");
        }

        [HttpGet("{eventoId}")]
        public async Task<IActionResult> Get(int eventoId)
        {
            try 
            {
                var evento = await this._repo.GetEventoAsyncById(eventoId, true);
                var results = this._mapper.Map<EventoDto>(evento);
                return Ok(results);

            }catch(Exception)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, "Banco de dados falhou");
            }
        }

        [HttpGet("getByTema/{tema}")]
        public async Task<IActionResult> Get(string tema)
        {
            try 
            {
                var eventos = await this._repo.GetAllEventosAsyncByTema(tema, true);
                var results = this._mapper.Map<EventoDto[]>(eventos);
                return Ok(results);

            }catch(Exception)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, "Banco de dados falhou");
            }
        }

        // qnd o parâmetro for entidade não precisa especificar na anotação
        [HttpPost]
        // public async Task<IActionResult> Post(Evento model)
        public async Task<IActionResult> Post(EventoDto model)
        {
            try 
            {
                /*
                    no POST faremos o mapeamento reverso, no get o mapeamento era do
                    domínio para o dto, no post vai ser do dto para o domínio
                    para o mapeamento funcionar eh preciso colocar o domínio como generics
                    de Map, e passar o dto como parâmetro

                    porém surge um problema, no domínio Evento tem PalestranteEvento e no dto
                    EventoDto tem Palestrante, logo dará erro, para evitar isso eh necessário
                    estabelecer um mapeamento reverso no arquivo AutoMapperProfiles
                */

                var evento = this._mapper.Map<Evento>(model);

                this._repo.Add(evento);

                //this._repo.Add(model);

                if(await this._repo.SaveChangesAsync()) {

                   /*
                        ao passar a rota abaixo qnd o evento for criado o usuário será redirecionado
                        para a rota que exibirá esse evento criado : Get(int eventoId)
                   */

                   // return Created($"/api/evento/{model.Id}", model);
                   
                   /*
                        passo o mapeamento no retorno, pois na adição ao bd eh adicionado um 
                        domínio, logo fazendo o mapeamento abaixo faço um match entre domínio
                        e dto, evitando futuros problemas
                   */

                   return Created($"/api/evento/{model.Id}",  this._mapper.Map<EventoDto>(evento));
                }
                
            }catch(Exception ex)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, 
                                      $"Banco de dados falhou {ex.Message}");
            }

            return BadRequest();
        }

        [HttpPut("{eventoId}")]
        // public async Task<IActionResult> Put(int eventoId, Evento model)
        public async Task<IActionResult> Put(int eventoId, EventoDto model)
        {
            try 
            {

                var evento = await this._repo.GetEventoAsyncById(eventoId, false);

                if(evento == null) return NotFound();
                
                /*
                    pelo fato de eu estar usando o AsNoTracking para evitar possíveis problemas
                    de atualização ou deleção, n será possível deletar as informações referentes a 
                    lotes e redes sociais qnd elas n forem enviadas no painel de atualização dos 
                    dados, logo teremos q encontrar uma maneira de deletar esses relacionamentos
                    com Evento

                    OBS: O AsNoTracking remove o binding entre 
                    this._repo.GetEventoAsyncById(eventoId, false) e var evento
                */

                var idLotes = new List<int>();
                var idRedesSociais = new List<int>();

                // foreach(var item in model.Lotes)
                //     idLotes.Add(item.Id);
                
                // foreach(var item in model.RedesSociais)
                //     idRedesSociais.Add(item.Id);

                model.Lotes.ForEach(item => idLotes.Add(item.Id));
                model.RedesSociais.ForEach(item => idRedesSociais.Add(item.Id));

                 /*
                    aqui pego somente os lotes com ids q n estão em idLotes, ou seja, pego os
                    ids dos lotes q o usuário removeu do front-end
                */
                
                // var lotes = evento.Lotes.Where(lote => !idLotes.Contains(lote.Id)).ToList<Lote>();
                // var redesSociais = evento.RedesSociais.Where(redeSocial => !idRedesSociais.Contains(redeSocial.Id)).ToList<RedeSocial>();

                var lotes = evento.Lotes.Where(lote => !idLotes.Contains(lote.Id)).ToArray();
                var redesSociais = evento.RedesSociais.Where(redeSocial => !idRedesSociais.Contains(redeSocial.Id)).ToArray();

                if(lotes.Length > 0) {
                    /*
                        removo do bd os lotes q foram removidos do front
                        Delete n aceita array, por isso fiz um forEach passando o objeto do
                        tipo Lotes
                    */
                    
                    // lotes.ForEach(lote => this._repo.Delete(lote));

                    this._repo.DeleteRange(lotes);
                }

                if(redesSociais.Length > 0) {
                    // redesSociais.ForEach(redeSocial => this._repo.Delete(redeSocial));
                    
                    this._repo.DeleteRange(redesSociais);
                }

                /*
                    coloco a linha abaixo, para ao fazer o mapeamento do dto (model) substituir os 
                    dados que estão no bd (q evento armazena) pelos dados q o dto armazena
                */

                this._mapper.Map(model, evento);

                this._repo.Update(evento); // evento já armazena os dados de model

                //this._repo.Update(model);

                if(await this._repo.SaveChangesAsync())
                   return Created($"/api/evento/{model.Id}", this._mapper.Map<EventoDto>(evento));
                                      
                // return Created($"/api/evento/{model.Id}", model);
                
            }catch(Exception)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, "Banco de dados falhou");
            }

            return BadRequest();
        }

        [HttpDelete("{eventoId}")]
        public async Task<IActionResult> Delete(int eventoId)
        {
            try 
            {
                var evento = await this._repo.GetEventoAsyncById(eventoId, false);

                if(evento == null) return NotFound();

                this._repo.Delete(evento);

                if(await this._repo.SaveChangesAsync())
                   return Ok();
                
                
            }catch(Exception)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, "Banco de dados falhou");
            }

            return BadRequest();
        }
    }
}