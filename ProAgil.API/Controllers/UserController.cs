using System.Text;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ProAgil.API.Dtos;
using ProAgil.Domain.Identity;

/*
    IConfiguration deve ser importado de Microsoft.Extensions.Configuration e não de
    AutoMapper.Configuration
*/
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;

namespace ProAgil.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IConfiguration _config;
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;
        private readonly IMapper _mapper;

        public UserController(IConfiguration config, UserManager<User> userManager,
                              SignInManager<User> signInManager, IMapper mapper)
        {
            this._config        = config;
            this._userManager   = userManager;
            this._signInManager = signInManager;
            this._mapper        = mapper;
        }

        [HttpGet("GetUser")]
        public async Task<IActionResult> GetUser()
        {
            return Ok(new UserDto());
        }

        [HttpPost("Register")]

        /*
            como definimos uma política lá no Startup, lá no mvc, sem precisar usar anotação,
            de q eh necessário estar autenticado, logado para acessar as rotas, precisamos abrir
            uma exceção para essa rota de registro, por isso através da anotação abaixo qualquer
            usuário autenticado ou não acessará a rota de registro

            OBS: na criação do usuário pelo POSTMAN eh demonstrado que a senha inserida pelo usuário
                 no registro, não será mostrada ao ser retornado o UserDto após a criação do usuário 
                 no bd
        */

        [AllowAnonymous]
        public async Task<IActionResult> Register(UserDto userDto)
        {
            try 
            {
                // mapeamento dto para domínio
                var user = this._mapper.Map<User>(userDto);

                // crio o usuário no bd
                var result = await this._userManager.CreateAsync(user, userDto.Password);

                /*
                    faço o mapeamento reverso, de domínio para dto para confirmar o match, evitando
                    quaisquer problemas de incompatibilidade e pq eu n posso retornar um User devido
                    ao fato de ter campos q n são necessários
                */
                var userToReturn = this._mapper.Map<UserDto>(user);

                if(result.Succeeded)
                    return Created("GetUser", userToReturn);
                
                return BadRequest(result.Errors);

            }catch(Exception exception)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, 
                                       $"Banco de dados falhou {exception.Message}");
            }
        }

        [HttpPost("Login")]
        [AllowAnonymous]
        public async Task<IActionResult> Login(UserLoginDto userLoginDto)
        {
            try 
            {   
                // se o nome de usuário for válido, existir no bd passa pra próxima linha
                var user = await this._userManager.FindByNameAsync(userLoginDto.UserName);

                /*
                    verifica se a senha passada pelo usuário existe no bd
                    o terceiro parâmetro q eh false, impede de bloquear o usuário caso ele
                    passe a senha errada
                */
                var result = await this._signInManager.CheckPasswordSignInAsync(user, 
                             userLoginDto.Password, false);

                if(result.Succeeded)
                {
                    // da entidade Users pego somente o usuário q passou seu nome de usuário
                    var appUser = await this._userManager.Users
                                    .FirstOrDefaultAsync(u => 
                                    u.NormalizedUserName == userLoginDto.UserName.ToUpper());

                    var userToReturn = this._mapper.Map<UserLoginDto>(appUser);

                    // retorno para o usuário o seu token de acesso e suas informações
                    return Ok(new {
                        token = GenerateJWToken(appUser).Result, // gero o token do usuário logado
                        user = userToReturn // retorno userName e senha
                    });
                }

                return Unauthorized();

            }catch(Exception exception)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, 
                                       $"Banco de dados falhou {exception.Message}");
            }
        }

        private async Task<string> GenerateJWToken(User user)
        {
            /*
                poderia fazer: 
                var claims = new List<CLaim>;
                claims.Add(new Claim(ClaimTypes.NameIdentifier, user.Id.Name));

                porém como estamos usando c# eh possível criar uma instância de forma mais
                direta, fazendo o comando abaixo
            */
            var claims = new List<Claim>
            {
                new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()),
                new Claim(ClaimTypes.Name, user.UserName)
            };

            // pego todas as permissões do usuário
            var roles = await this._userManager.GetRolesAsync(user);

            foreach(var role in roles)
            {
                claims.Add(new Claim(ClaimTypes.Role, role));
            }

            var secretKey = new SymmetricSecurityKey(Encoding.ASCII
                                .GetBytes(this._config.GetSection("AppSettings:Token").Value));
            
            // algoritmo de encodamento, ou seja, aqui eh feito o encodamento do token
            var creds     = new SigningCredentials(secretKey, SecurityAlgorithms.HmacSha512Signature); 

            var tokenDescriptor = new SecurityTokenDescriptor{
                Subject            = new ClaimsIdentity(claims),
                Expires            = DateTime.Now.AddDays(1),
                SigningCredentials = creds
            };

            var tokenHandler = new JwtSecurityTokenHandler();

            // cria o token
            var token        =  tokenHandler.CreateToken(tokenDescriptor);

            // retorno o token
            return tokenHandler.WriteToken(token);
        }
    }
}