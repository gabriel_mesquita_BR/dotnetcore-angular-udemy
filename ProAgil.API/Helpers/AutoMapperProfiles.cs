using System.Linq;
using AutoMapper;
using ProAgil.API.Dtos;
using ProAgil.Domain;
using ProAgil.Domain.Identity;

namespace ProAgil.API.Helpers
{
    /*
        ao criar uma classe automapper q eh do tipo Profile, qnd referenciarmos Profile dentro da
        classe Startup automaticamente o auto mapper irá verificar se no projeto possui 
        algum arquivo de perfil (profile) e assim encontrará essa classe, n precisando nós 
        referenciá-la em nenhum lugar do projeto
    */

    public class AutoMapperProfiles : Profile
    {
        public AutoMapperProfiles() 
        {
            // fazendo isso estabelece-se a ligação entre o domínio e o dto

            /*
                Como Evento e Palestrante possuem multiplicidade many to many, temos q fazer essa
                configuração no auto mapper

                Evento (q eh do domínio) armazenará os dados presentes no bd referente a eventos, 
                EventoDto armazenará os dados de Evento sendo feita uma filtragem selecionando
                somente os dados q eu quiser enviar para a interface (front), esses dados q eu
                quero enviar para a interface são as propriedades de EventoDto

                resumindo o q estou querendo dizer abaixo eh:
                no meu dest (destinatário) q eh EventoDto pego os Palestrantes, nas opções o
                mapeamento virá de src (source) (q significa origem), ou seja, virá de Evento,
                nele acesso PalestrantesEventos, em PalestrantesEventos seleciono os palestrantes
                convertendo para lista
            */

            CreateMap<Evento, EventoDto>()
                .ForMember(dest => dest.Palestrantes, opt => {
                    opt.MapFrom(src => src.PalestrantesEventos.Select(x => x.Palestrante).ToList());
            }).ReverseMap();

            /*
                existem 2 formas de fazer mapeamento reverso, uma eh o exemplo comentado e a outra
                eh adicionar .ReverseMap()
            */

            // CreateMap<EventoDto, Evento>();
            
            CreateMap<Palestrante, PalestranteDto>()
                .ForMember(dest => dest.Eventos, opt => {
                    opt.MapFrom(src => src.PalestrantesEventos.Select(x => x.Evento).ToList());
            }).ReverseMap();
            
            // CreateMap<PalestranteDto, Palestrante>();
            
            CreateMap<Lote, LoteDto>().ReverseMap();

            // CreateMap<LoteDto, Lote>();

            CreateMap<RedeSocial, RedeSocialDto>().ReverseMap();

            // CreateMap<RedeSocialDto, RedeSocial>();

            CreateMap<User, UserDto>().ReverseMap();

            // CreateMap<User, UserLoginDto>().ReverMap();
        }
    }
}