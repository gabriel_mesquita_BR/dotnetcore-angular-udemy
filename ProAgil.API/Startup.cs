﻿using System.Text;
using System.IO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using ProAgil.Repository;
using Microsoft.Extensions.FileProviders;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using ProAgil.Domain.Identity;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
//using ProAgil.API.Data;

namespace ProAgil.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            services.AddMvc(options => {

                /*
                    toda vez q uma rota for acessada o services.AddMvc eh chamado
                    ao passar os comandos abaixo estou dizendo q para acessar qualquer rota
                    eh necessário ter uma autenticação, ou seja, está logado
                */

                var policy = new AuthorizationPolicyBuilder()
                                .RequireAuthenticatedUser()
                                .Build();
                
                // eh feita uma filtragem passado um AuthorizeFilter
                options.Filters.Add(new AuthorizeFilter(policy));

            }).SetCompatibilityVersion(CompatibilityVersion.Version_2_2)
              .AddJsonOptions(opt => 
                {
                    /*
                        ao colocar o comando abaixo evito q surjam erros devido ao loop entre
                        as entidades relacionadas, por exemplo:

                        um evento tem um lote, um lote tem um evento, um evento tem um lote....
                        como forma de resolver isso retiramos a palavra chave "set" para evitar
                        setar, outra forma de resolver esse problema, de forma mais padronizada
                        eh colocar o comando abaixo
                    */

                    opt.SerializerSettings.ReferenceLoopHandling = 
                    Newtonsoft.Json.ReferenceLoopHandling.Ignore;
                });

            services.AddDbContext<ProAgilContext>(
                options => options.UseMySql(Configuration.GetConnectionString("DefaultConnection"))
            );

            /*
                para usarmos o Identity, autenticação, autorização, eh necessário fazermos algumas
                modificações
            */

            IdentityBuilder builder = services.AddIdentityCore<User>(options => 
                {
                    // abaixo fazemos configuração referente a senha
                    options.Password.RequireDigit           = false;
                    options.Password.RequireNonAlphanumeric = false;
                    options.Password.RequireLowercase       = false;
                    options.Password.RequireUppercase       = false;
                    options.Password.RequiredLength         = 4;
                }
            );



            /*
                builder.UserType será do tipo Role, pois User se relaciona com Role
                builder.Services eh o builder criados por nós acima
            */

            builder = new IdentityBuilder(builder.UserType, typeof(Role), builder.Services);
            
            // EntityFrameworkStores usará o contexto do bd configurado por nós
            builder.AddEntityFrameworkStores<ProAgilContext>();

            builder.AddRoleValidator<RoleValidator<Role>>();
            builder.AddRoleManager<RoleManager<Role>>();
            builder.AddSignInManager<SignInManager<User>>();

            // configurando JWT
            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(options =>

                {
                    options.TokenValidationParameters = new TokenValidationParameters 
                    {      
                        // estou validando a chave do emissor (q eh a nossa API)
                        ValidateIssuerSigningKey = true,

                        /*
                            pego o valor da chave secreta em appsettings, sendo q estará dentro da 
                            chave AppSettings e dentro de outra chave chamada Token
                        */

                        IssuerSigningKey         = new SymmetricSecurityKey(Encoding.ASCII
                                                    .GetBytes(Configuration
                                                        .GetSection("AppSettings:Token").Value)),
                        // quem fornece este token
                        ValidateIssuer           = false, // n quero enviar o emissor (API)
                        // a quem se destina este token
                        ValidateAudience         = false // n quero enviar a quem se destina essa API
                    };
                }
            );

            /*
                ao colocar o comando abaixo estou dizendo q qnd um controller injetar como 
                dependência a interface IProAgilRepository, será injetado a classe ProAgilRepository
                que extende a interface IProAgilRepository, com todos aqueles métodos implementados
                e conexão com o bd

                agr o controller que injetar a interface IProAgilRepository requisitará a classe
                ProAgilRepository e n mais a classe ProAgilContext
            */

            services.AddScoped<IProAgilRepository, ProAgilRepository>();

            /*
                ao referenciar auto mapper aqui em Startup n eh preciso referenciar as classes
                q são do tipo perfil (Profile), pois o auto mapper irá encontrá-las automaticamente

                a versão do auto mapper usada no curso foi a 4.0, nessão versão n precisou usar
                typeof(Startup) como parâmetro de services.AddAutoMapper

                porém, na versão do auto mapper usada por mim, q foi a 7.0 preciou colocar 
                typeof(Startup), se n dava erro
            */
            services.AddAutoMapper(typeof(Startup));

            // para evitar erro de cors
            services.AddCors();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            // eh necessário para a autorização (JWT) funcionar
            app.UseAuthentication();

            //comento essa linha para usar o protocolo http
            //app.UseHttpsRedirection();
            
            // eh preciso colocar essa linha antes de começar a usar o MVC
            app.UseCors(x => x.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader());

            // para ser possível usar arquivos estáticos como js, css, imagens
            // ao colocar esse comando, por padrão vc acessa o diretório wwwroot
            app.UseStaticFiles();

            /*
                para o upload das imagens serem feitas com sucesso em Resources/Images eh
                necessário a configuração abaixo

                DICA: quando eu faço: new StaticFileOptions() {}, estou instanciando essa
                      classe sem precisar criar um objeto
            */

            app.UseStaticFiles(new StaticFileOptions() {
                // no curso tem @"Resources"
                // aqui vc especifica onde as imagens ficarão
                FileProvider = new PhysicalFileProvider(Path.Combine(Directory.GetCurrentDirectory(), "Resources")),
                
                // aqui vc especifica onde as imagens serão salvas após a requisição
                RequestPath  = new PathString("/Resources")
            });
    
            app.UseMvc();
        }
    }
}
