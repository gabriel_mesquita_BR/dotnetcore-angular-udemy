using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using ProAgil.Domain;
using ProAgil.Domain.Identity;
//using ProAgil.API.Models;

// namespace ProAgil.API.Data // antes ficava em webapi e no diretório Data

namespace ProAgil.Repository
{
    //classe de configuração do bd
    //public class ProAgilContext : DbContext

    /*
        para efetuar o relacionamento de User, Role e UserRole n faremos como fizemos nas 
        entidades criadas por nós anteriormente usando DbSet, pois entidades Identity tem
        uma forma diferente
        ao passar User, Role, UserRole, tipo da chave (int), IdentityUserClaim<int>,......
        dessa forma será feito o relacionamento, será criada as tabelas no bd
    */
    public class ProAgilContext : IdentityDbContext<User, Role, int, IdentityUserClaim<int>,
                                                    UserRole, IdentityUserLogin<int>,
                                                    IdentityRoleClaim<int>, IdentityUserToken<int>>
    {
        //precisamos passar a conexão para o construtor da classe mãe, por isso o uso do base
        public ProAgilContext(DbContextOptions<ProAgilContext> options) : base(options) {}

        //para o ef converter a nossa entidade Evento em uma tabela chamada eventos
        public DbSet<Evento> Eventos {get; set;}

        public DbSet<Palestrante> Palestrantes {get; set;}

        public DbSet<PalestranteEvento> PalestrantesEventos {get; set;}

        public DbSet<Lote> Lotes {get; set;}

        public DbSet<RedeSocial> RedesSociais {get; set;}

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // para completar o relacionamento e criação das tabelas referente ao Identity
            // passo para o construtor da classe mãe (IdentityDbContext) modelBuilder
            
            base.OnModelCreating(modelBuilder);    

            modelBuilder.Entity<UserRole>(userRole => {
                
                userRole.HasKey(ur => new {ur.UserId, ur.RoleId});

                /*
                    ao fazer relacionamento many to many em Identity, mais especificadamente em
                    User e Role, n basta passa o comando acima como fizemos nas outras entidades, 
                    tem q colocar os comandos abaixo

                    userRole tem uma role, essa role tem mts userRoles (pois uma role pode ter 
                    vários usuários), userRole tem uma chave estrangeira chamada RoleId, e essa
                    chave é obrigatória, ou seja, tem q ter o Id de Role
                */

                userRole.HasOne(ur => ur.Role)
                    .WithMany(r => r.UserRoles)
                    .HasForeignKey(ur => ur.RoleId)
                    .IsRequired();

                userRole.HasOne(ur => ur.User)
                    .WithMany(u => u.UserRoles)
                    .HasForeignKey(ur => ur.UserId)
                    .IsRequired();
            });

            // =================================================================================== //

            /*
                método usado para fazer alterações nas entidades q refletirão nas tabelas
                abaixo estou definindo as chaves primárias da tabela palestrantesEventos
            */
            modelBuilder.Entity<PalestranteEvento>()
                .HasKey(PE => new {PE.EventoId, PE.PalestranteId});
        }

    }
}