using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using ProAgil.Domain;

namespace ProAgil.Repository
{
    public class ProAgilRepository : IProAgilRepository
    {
        private readonly ProAgilContext _context;

        /*
            TRACKING VS NO TRACKING

            os exemplos mais comuns de ocorrerem problemas por causa da falta de mapeamento e 
            consequentemente pelo tracking do ef estar ativado eh no PUT e DELETE

            qnd for fazer uma busca antes de atualizar ou deletar, pode ser q o tracking esteja
            ativo e assim o ef irá travar o seu recurso, impossibilitando a atualização ou deleção
            ser feita, travando toda a sua aplicação, por isso existem 2 formas de tirar o
            tracking (rastreamento), uma geral e outra específica
        */

        public ProAgilRepository(ProAgilContext context)
        {
            this._context = context;

            //Forma Geral de remover o tracking
            this._context.ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;
        }

        public void Add<T>(T entity) where T : class
        {
            this._context.Add(entity);
        }

        public void Update<T>(T entity) where T : class
        {
            this._context.Update(entity);
        }

        public void Delete<T>(T entity) where T : class
        {
            this._context.Remove(entity);
        }

        public void DeleteRange<T>(T[] entityArray) where T : class
        {
            this._context.RemoveRange(entityArray);
        }
        /*
            Ao utilizar o async em operações envolvendo o bd, primeiro vc garante q as
            próximas linhas de código só serão executadas após terminar a operação com o
            bd e vc garante q o bd não trave, ou seja, poderá continuar realizando outras
            operações para outras aplicações sem problema
        */

        public async Task<bool> SaveChangesAsync()
        {
            //se for maior que zero eh pq foi salvo com sucesso as alterações
            return (await this._context.SaveChangesAsync()) > 0;
        }

        //Eventos

        public async Task<Evento[]> GetAllEventosAsync(bool includePalestrantes = false)
        {   
            //passo no Include as entidades que relacionam com Evento
            IQueryable<Evento> query = this._context.Eventos
                .Include(lote => lote.Lotes)
                .Include(redesocial => redesocial.RedesSociais);

            if(includePalestrantes)
            {
                /*
                    ao fazer ThenInclude vc continua trabalhando na msm entidade q o Include anterior,
                    por isso ao fazer ThenInclude acesso a entidade PalestrantesEventos e pego o
                    Palestrante, pq agora sim pego os palestrantes
                */

                query = query
                    .Include(pe => pe.PalestrantesEventos)
                    .ThenInclude(p => p.Palestrante);
            }

            /*
                qnd vc tem um retorno específico em uma operação no bd, como eh o caso deste
                método, em q o retorno eh de um Evento, para retirar o tracking basta ir na última
                query e colocar AsNoTracking()
            */

            query = query
                .AsNoTracking()
                .OrderBy(e => e.Id);

            return await query.ToArrayAsync();
        }

        public async Task<Evento[]> GetAllEventosAsyncByTema(string tema, bool includePalestrantes = false)
        {
            IQueryable<Evento> query = this._context.Eventos
                .Include(lote => lote.Lotes)
                .Include(redesocial => redesocial.RedesSociais);

            if(includePalestrantes)
            {
                query = query
                    .Include(pe => pe.PalestrantesEventos)
                    .ThenInclude(p => p.Palestrante);
            }

            query = query
                .AsNoTracking()
                .OrderByDescending(e => e.DataEvento)
                .Where(e => e.Tema.ToLower().Contains(tema.ToLower()));

            return await query.ToArrayAsync();
        }

        public async Task<Evento> GetEventoAsyncById(int eventoId, bool includePalestrantes = false)
        {
            IQueryable<Evento> query = this._context.Eventos
                .Include(lote => lote.Lotes)
                .Include(redesocial => redesocial.RedesSociais);

            if(includePalestrantes)
            {
                query = query
                    .Include(pe => pe.PalestrantesEventos)
                    .ThenInclude(p => p.Palestrante);
            }

            query = query
                .AsNoTracking()
                .OrderBy(e => e.Id)
                .Where(e => e.Id == eventoId);

            return await query.FirstOrDefaultAsync();
        }

        //Palestrantes

        public async Task<Palestrante> GetPalestranteAsync(int palestranteId, bool includeEventos = false)
        {
            IQueryable<Palestrante> query = this._context.Palestrantes
                .Include(redesocial => redesocial.RedesSociais);

            if(includeEventos)
            {
                query = query
                    .Include(pe => pe.PalestrantesEventos)
                    .ThenInclude(e => e.Evento);
            }

            query = query.AsNoTracking().OrderBy(p => p.Nome).Where(p => p.Id == palestranteId);

            return await query.FirstOrDefaultAsync();
        }

        public async Task<Palestrante[]> GetAllPalestrantesAsyncByName(string name, bool includeEventos = false)
        {
            IQueryable<Palestrante> query = this._context.Palestrantes
                .Include(redesocial => redesocial.RedesSociais);

            if(includeEventos)
            {
                query = query
                    .Include(pe => pe.PalestrantesEventos)
                    .ThenInclude(e => e.Evento);
            }

            query = query.AsNoTracking().Where(p => p.Nome.ToLower().Contains(name.ToLower()));

            return await query.ToArrayAsync();
        }
    }
}