import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {JwtHelperService} from '@auth0/angular-jwt';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  baseUrl   = 'http://localhost:5000/api/user/';
  jwtHelper = new JwtHelperService(); // npm install @auth0/angular-jwt
  decodedToken: any;

  constructor(private http: HttpClient) { }

  login(model: any) {
    // uso o pipe para fazer o map (mapeamemento)
    return this.http.post(`${this.baseUrl}login`, model).pipe(

      map((response: any) => {
        const user = response;

        if (user) {

          /*
            uso localStorage e n cookie, pois o cookie eh possível pegar tendo acesso a máquina
            da pessoa, enquanto q o localStorage só eh possível acessá-lo através da url login, e para
            ter acesso a essa url eh necessário saber o userName e password do usuário
          */

          localStorage.setItem('token', user.token);

          /*
            essa parte de decodamento, só decodifica o header e o payload, pois a parte de 
            signature eh necessário possuir a chave secreta
          */

          this.decodedToken = this.jwtHelper.decodeToken(user.token);

          sessionStorage.setItem('username', this.decodedToken.unique_name);
        }
      })
    );
  }

  register(model: any) {
    return this.http.post(`${this.baseUrl}register`, model);
  }

  loggedIn() {
    const token = localStorage.getItem('token');

    /*
      token está expirado? false (retorna true (por causa do !)) true (retorna false (por causa do !))
      assim consigo mostrar ou esconder coisas dependendo se o usuário está logado ou n, dependendo
      se o token está expirado ou n

      nessa parte tbm ocorre decodamento, e tbm só decodifica o header e o payload, pois a parte de
      signature eh necessário possuir a chave secreta
    */
    return !this.jwtHelper.isTokenExpired(token);
  }
}
