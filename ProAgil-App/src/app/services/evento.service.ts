import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Evento } from '../models/Evento';

// com esse decorator é possível injetar esse serviço em algum lugar

@Injectable({
  providedIn: 'root'
})
export class EventoService {

  baseUrl = 'http://localhost:5000/api/evento';

  /*
    pelo fato de agr estar usando interceptor n eh mais necessário passar o token no header em cada
    requisição q seja necessário o usuário estar autorizado
  */

  // tokenHeader: HttpHeaders;

  constructor(private http: HttpClient) {
    // this.tokenHeader = new HttpHeaders({'Authorization': `Bearer ${localStorage.getItem('token')}`});
  }

  getAllEvento(): Observable<Evento[]> {
    // esse método get retornar um Observable (alternativa da Promise)
    // para acessar essa rota é necessário estar autorizado, logo eh necessário passar no cabeçalho
    // da requisição o token
    // return this.http.get<Evento[]>(this.baseUrl, {headers: this.tokenHeader});

    return this.http.get<Evento[]>(this.baseUrl);
  }

  getEventoByTema(tema: string): Observable<Evento[]> {
    // esse método get retornar um Observable (alternativa da Promise)
    return this.http.get<Evento[]>(`${this.baseUrl}/getByTema/${tema}`);
  }

  getEventoById(id: number): Observable<Evento> {
    // esse método get retornar um Observable (alternativa da Promise)
    return this.http.get<Evento>(`${this.baseUrl}/${id}`); 
  }

  postEvento(evento: Evento) {
    // esse método get retornar um Observable (alternativa da Promise)
    return this.http.post(this.baseUrl, evento);
  }

  postUpload(file: File, nomeArquivo: string) {
    const fileToUpload = file[0] as File; // como a imagem eh retornada como array, pego a primeira posição
    const formData     = new FormData();

    // passo o nome do formData, o arquivo a ser enviado para o dotnet e o nome do arquivo
    // formData.append('file', fileToUpload, fileToUpload.name);
    formData.append('file', fileToUpload, nomeArquivo);

    return this.http.post(`${this.baseUrl}/upload`, formData);
  }

  putEvento(evento: Evento) {
    return this.http.put(`${this.baseUrl}/${evento.id}`, evento);
  }

  deleteEvento(id: number) {
    return this.http.delete(`${this.baseUrl}/${id}`);
  }
}
