import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EventosComponent } from './eventos/eventos.component';
import { PalestrantesComponent } from './palestrantes/palestrantes.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ContatosComponent } from './contatos/contatos.component';
import { UserComponent } from './user/user.component';
import { LoginComponent } from './user/login/login.component';
import { RegistrationComponent } from './user/registration/registration.component';
import { AuthGuard } from './auth/auth.guard';
import { EventosEditComponent } from './eventos/eventosEdit/eventosEdit.component';

const routes: Routes = [

  /*
    em path vc passa o nome q será passado na uri como rota e ao ser passado esse nome
    o componente q estiver na chave component será carregado na view

    o componente user tem 2 componentes filhos (login e registration) por isso coloco os
    comandos abaixo, esses componentes filhos eh pq através de user acessaremos login e
    registration
  */

  {path: 'user', component: UserComponent,
   children: [
    {path: 'login', component: LoginComponent},
    {path: 'registration', component: RegistrationComponent}
   ]
  },

  /*
    colocando canActivate: [AuthGuard] estou usando o Guard do Angular para tornar as
    rotas protegidas contra usuários q n estão autorizados, ou seja, n estão autenticados e sem
    o token
  */

  {path: 'eventos', component: EventosComponent, canActivate: [AuthGuard]},
  {path: 'evento/:id/edit', component: EventosEditComponent, canActivate: [AuthGuard]},
  {path: 'palestrantes', component: PalestrantesComponent, canActivate: [AuthGuard]},
  {path: 'dashboard', component: DashboardComponent, canActivate: [AuthGuard]},
  {path: 'contatos', component: ContatosComponent, canActivate: [AuthGuard]},


  // se n passar nenhum nome na uri, deixar vazio, eh redirecionado para dashboard
  {path: '', redirectTo: 'dashboard', pathMatch: 'full'},

  // se passar algum nome na uri, q n corresponda com os paths anteriores, eh redirecionado para
  // dashboard
  {path: '**', redirectTo: 'dashboard', pathMatch: 'full'},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
