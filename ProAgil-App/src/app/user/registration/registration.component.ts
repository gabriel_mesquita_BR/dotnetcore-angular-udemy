import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { User } from 'src/app/models/User';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {

  registerForm: FormGroup;
  user: User;

  constructor(private authService: AuthService, private router: Router, public fb: FormBuilder) { }

  ngOnInit() {
    this.validation();
  }

  validation() {
    this.registerForm = this.fb.group({
      fullName: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      userName: ['', Validators.required],

      // passwords é um agrupador, q engloba senha e confirmar senha
      passwords: this.fb.group({
        password: ['', [Validators.required, Validators.minLength(4)]],
        confirmPassword: ['', Validators.required]
      }, {validator: this.compararSenhas}),
    });
  }

  compararSenhas(fb: FormGroup) {
    const confirmarSenha = fb.get('confirmPassword'); // pego o valor do campo de confirmação de senha

    // se o campo confirmar senha n tiver erros ou n tiver incompatibilidade
    // se colocar mismatch entre aspas simples, tem q colocar mismatch em setErrors e no template
    if (confirmarSenha.errors == null || 'mismatch' in confirmarSenha.errors) {

      // se o campo senha for diferente do campo confirmar senha
      if (fb.get('password').value !== confirmarSenha.value) {
        // existe incompatibilidade entre o campo confirmar senha e o campo senha
        confirmarSenha.setErrors({mismatch: true});
      } else {
        confirmarSenha.setErrors(null); // campo confirmar senha e senha são iguais
      }
    }
  }

  cadastrarUsuario() {

    if (this.registerForm.valid) {

      /*
        concateno senha com os valores do formulário, como senha está dentro de um agrupador
        passwords então o valor de password n vem ao passar this.registerForm.value
      */
      this.user = Object.assign({password: this.registerForm.get('passwords.password').value},
                                this.registerForm.value);

      this.authService.register(this.user).subscribe(

        () => {
          this.router.navigate(['/user/login']);
        },
        error => {
          const erro = error.error;
          erro.forEach(element => {
            switch(element.code) {

              case 'DuplicateUserName':
                console.log('Cadastro Duplicado');
                break;
              default:
                console.log(`Erro no cadastro! Code: ${element.code}`);
                break;
            }
          });
        }
      );
    }
  }

}
