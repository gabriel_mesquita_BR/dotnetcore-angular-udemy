// criamos esse arquivo para interceptar quaisquer requisição HTTP que o cliente fizer a nossa API

import {Injectable} from '@angular/core';
import { HttpInterceptor, HttpHandler, HttpRequest, HttpEvent } from '@angular/common/http';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';

// eh necessário colocar o caminho completo para evitar dar conflito com o Observable
import { tap } from 'rxjs/internal/operators/tap';

// injetamos esse nosso arquivo na raiz do nosso projeto, ou seja, em todo o projeto
@Injectable({
    providedIn: 'root'
})

export class AuthInterceptor implements HttpInterceptor {

    constructor(private router: Router) {}

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

        if (localStorage.getItem('token') !== null) {

            // clono a requisição feita pelo cliente e adiciono no cabeçalho da requisição o token
            const cloneReq = req.clone({
                headers: req.headers.set('Authorization', `Bearer ${localStorage.getItem('token')}`)
            });

            return next.handle(cloneReq).pipe(

                tap(
                    success => {},
                    error   => {
                        if (error.status === 401) {
                            this.router.navigateByUrl('user/login');
                        }
                    }
                )
            );
        } else {
            /*
                se o token for nulo, quer dizer q o usuário n está autorizado, então eu simplesmente
                retorno a requisição clonada sem adicionar o token no cabeçalho da requisição
            */
            return next.handle(req.clone());
        }
    }
}
