// ng g g auth/auth
// ng generate guard auth (diretório criado por nós) / auth
// o guard do angular evitará que rotas que só devem ser acessadas se o usuário estiver autorizado,
// ou seja, estiver autenticado e possuir o token, sejam acessadas sem esses pré-requisitos

import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, CanActivate, Router } from '@angular/router';
// import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(private router: Router) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {

    if (localStorage.getItem('token') !== null) {
      return true;
    } else {
      this.router.navigate(['/user/login']);
      return false;
    }
  }

}
