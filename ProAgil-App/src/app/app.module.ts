import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import {TooltipModule, BsDropdownModule, ModalModule, BsDatepickerModule, TabsModule} from 'ngx-bootstrap';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

// import {ToastrModule} from 'ngx-toastr';

import {NgxMaskModule} from 'ngx-mask';
import {NgxCurrencyModule} from 'ngx-currency';

import { AppComponent } from './app.component';
import { EventosComponent } from './eventos/eventos.component';
import { NavComponent } from './nav/nav.component';

import { DateTimeFormatPipePipe } from './pipes/dateTimeFormatPipe.pipe';
import { PalestrantesComponent } from './palestrantes/palestrantes.component';
import { ContatosComponent } from './contatos/contatos.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { TituloComponent } from './titulo/titulo.component';
import { UserComponent } from './user/user.component';

/*
   ao criar um componente dentro do outro, esses componentes filhos n serão importados
   aqui no módulo principal, vc precisa importá-los manualmente
*/
import { LoginComponent } from './user/login/login.component';
import { RegistrationComponent } from './user/registration/registration.component';
import { AuthInterceptor } from './auth/auth.interceptor';
import { EventosEditComponent } from './eventos/eventosEdit/eventosEdit.component';

@NgModule({
   declarations: [
      AppComponent,
      EventosComponent,
      NavComponent,
      DateTimeFormatPipePipe, // eh necessário importar o pipe criado,
      PalestrantesComponent,
      ContatosComponent,
      DashboardComponent,
      TituloComponent,
      UserComponent,
      LoginComponent,
      RegistrationComponent,
      EventosEditComponent
   ],
   imports: [
      BrowserModule,
      /*
         para o dropdowndown ngx-bootstrap funcionar n basta importar o módulo BsDropdownModule,
         é necessário também importar o módulo BrowserAnimationsModule
      */
     BrowserAnimationsModule,
     // ToastrModule.forRoot()
     BsDropdownModule.forRoot(), // ao colocar forRoot() será possível usar em todo o projeto
     BsDatepickerModule.forRoot(),
     TooltipModule.forRoot(),
     ModalModule.forRoot(),
     NgxMaskModule.forRoot(),
     NgxCurrencyModule,
     TabsModule.forRoot(),
     AppRoutingModule,
     HttpClientModule, // ao colocar esse módulo eh possível fazer requisições http
     FormsModule,

      // é necessário importar esse módulo para trabalhar com FormGroup
      ReactiveFormsModule,
   ],
   providers: [

      /*
         caso não queira usar o decorator @Injectable para tornar o serviço injetável em qualquer
         lugar do projeto, tem 2 opções:

         1 eh colocar ele nesse array de providers e outra eh criar um array providers no componente
         q vc queira importar esse service
      */

      // EventoService

      // eh necessário colocar esse objeto para usar o interceptor

      {
         provide: HTTP_INTERCEPTORS,
         useClass: AuthInterceptor,

         /*
            aqui estou dizendo q será possível tratar múltiplas requisições, pois estou usando o
            tap dentro de pipe, lá no auth.interceptor.ts
         */
         multi: true
      }
   ],
   bootstrap: [
      AppComponent
   ]
})
export class AppModule { }
