import { Component, OnInit } from '@angular/core';
import { EventoService } from 'src/app/services/evento.service';
import { BsModalService, BsLocaleService } from 'ngx-bootstrap';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { Evento } from 'src/app/models/Evento';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-eventos-edit',
  templateUrl: './eventosEdit.component.html',
  styleUrls: ['./eventosEdit.component.css']
})
export class EventosEditComponent implements OnInit {

  titulo = 'Editar Evento';

  /*
    ao inicializar o evento como um objeto, como Evento era uma interface seria necessário passar
    no objeto os atributos da interface para evitar erro
  */

  // evento: Evento = {};
  evento: Evento = new Evento();
  urlImagem = 'assets/img/upload.png';
  registerForm: FormGroup;
  fileNameToUpdate: string;
  dataAtual: string;
  file: File;

  get lotes(): FormArray {
    return this.registerForm.get('lotes') as FormArray;
  }

  get redesSociais(): FormArray {
    return this.registerForm.get('redesSociais') as FormArray;
  }

  constructor(private eventoService: EventoService, private modalService: BsModalService,
              private fb: FormBuilder, private localeService: BsLocaleService,
              private router: ActivatedRoute) {

      this.localeService.use('pt-br'); // faço isso para usar a data no formato português
  }

  ngOnInit() {
    this.validation();
    this.carregarEvento();
  }

  carregarEvento() {
    // pego o id q estiver sendo passado pela rota evento/:id/edit
    // this.router.snapshot.paramMap.get('id') retornará uma string, para convertermos para
    // number basta colocarmos o sinal de + na frente
    const idEvento = +this.router.snapshot.paramMap.get('id');

    // como getEventoById retornar um Observable para executarmos precisamos usar o subscribe
    this.eventoService.getEventoById(idEvento).subscribe(
      // esse parâmetro evento, armazena o retorno de getEventoById
      (evento: Evento) => {
        this.evento = Object.assign({}, evento);
        this.fileNameToUpdate = evento.urlImagem.toString();

        this.urlImagem = `http://localhost:5000/resources/images/${this.evento.urlImagem}?_ts=${this.dataAtual}`;

        // passando string vazia para urlImagem evitará um erro dizendo q n existe value pro input
        // eh necessário deixar nulo, pois o input do tipo file só eh get, n eh set, ou seja, n
        // consegue setar nenhum dado para urlImagem
        this.evento.urlImagem = '';

        this.registerForm.patchValue(this.evento);

        this.evento.lotes.forEach(lote => this.lotes.push(this.criaLote(lote)));
        this.evento.redesSociais.forEach(redeSocial => this.redesSociais.push(this.criaRedeSocial(redeSocial)));
      }
    );
  }

  validation() {
    // this.registerForm = new FormGroup({

    this.registerForm = this.fb.group({

      id: [],
      tema: ['', [Validators.required, Validators.minLength(4), Validators.maxLength(50)]],
      local: ['', Validators.required],
      dataEvento: ['', Validators.required],
      urlImagem: [''],
      qtdPessoas: ['', [Validators.required, Validators.max(120000)]],
      telefone: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],

      /*
        dessa forma dizemos q lotes e redes sociais são um array, pois posso ter vários lotes e 
        várias redes sociais
      */

      // lotes: this.fb.array([this.criaLote()]),
      // redesSociais: this.fb.array([this.criaRedeSocial()])

      /*
        removemos this.criaLote() e this.criaRedeSocial(), pois aqui só ocorre a validação, n eh
        aqui q será criado lotes ou redes sociais
      */

      lotes: this.fb.array([]),
      redesSociais: this.fb.array([])

      // lotes: this.criaLote(),
      // redesSociais: this.criaRedeSocial()
    });
  }

  criaLote(lote: any): FormGroup {

    return this.fb.group({

      /*
        o primeiro parâmetro eh o q vc quer q aconteça assim q o campo for carregado na view
        no nosso caso queremos passar informações para os campos

        precisamos passar o id para termos a referência de qual lote estamos passando os dados
      */

      id: [lote.id],
      nome: [lote.nome, Validators.required],
      quantidade: [lote.quantidade, Validators.required],
      preco: [lote.preco, Validators.required],
      dataInicio: [lote.dataInicio],
      dataFim: [lote.dataFim],
    });
  }

  criaRedeSocial(redeSocial: any): FormGroup {

    return this.fb.group({
      id: [redeSocial.id],
      nome: [redeSocial.nome, Validators.required],
      url: [redeSocial.url, Validators.required],
    });
  }

  // aqui a imagem eh trocada

  onFileChange(file: FileList) {

    const reader = new FileReader();
    reader.onload = (event: any) => this.urlImagem = event.target.result;
    this.file = event.target.files;
    reader.readAsDataURL(file[0]);
  }

  /*
    passamos um objeto tendo como único dado o id com valor zero, pois na criação de um
    lote o id eh zero, pois aquele lote ainda n existe e temos q passar o id para q n ocorra erros
    no método this.criaLote(), o resto dos campos n precisa ter nenhum valor, eles podem ficar nulos
  */

  adicionarLote() {
    this.lotes.push(this.criaLote({id: 0}));
  }

  adicionarRedeSocial() {
    this.redesSociais.push(this.criaRedeSocial({id: 0}));
  }

  removerRedeSocial(id: number) {
    this.lotes.removeAt(id);
  }

  removerLote(id: number) {
    this.redesSociais.removeAt(id);
  }

  salvarEvento() { // este método eh responsável por salvar o evento alterando informações já existentes

    this.evento = Object.assign({id: this.evento.id}, this.registerForm.value);

    // como deixei nulo this.evento.urlImagem, na hora de salvar no bd, no servidor, eh necessário
    // passar o nome da imagem
    this.evento.urlImagem = this.fileNameToUpdate;

    this.uploadImagem();

    this.eventoService.putEvento(this.evento).subscribe(
      () => console.log('Editado com sucesso'),
      error => console.log(error)
    );
  }

  uploadImagem() {

    // se houve alteração na imagem entro no if, pois pode ser q n queira mudar a img
    // aqui a imagem eh enviada para o servidor
    if (this.registerForm.get('urlImagem').value !== '') {

      this.eventoService.postUpload(this.file, this.fileNameToUpdate).subscribe(
        () => {
          this.dataAtual = new Date().getMilliseconds().toString();
          this.urlImagem = `http://localhost:5000/resources/images/${this.evento.urlImagem}?_ts=${this.dataAtual}`;
        }
      );
    }
  }
}
