import { Component, OnInit, TemplateRef } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { EventoService } from '../services/evento.service';
import { Evento } from '../models/Evento';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';

import {FormGroup, FormControl, Validators, FormBuilder} from '@angular/forms';

import {defineLocale, BsLocaleService, ptBrLocale} from 'ngx-bootstrap';

// import {ToastrService} from 'ngx-toastr';

// pede ao defineLocale para configurar o ptBrLocale como pt-br
defineLocale('pt-br', ptBrLocale);

@Component({
  selector: 'app-eventos',
  templateUrl: './eventos.component.html',
  styleUrls: ['./eventos.component.css']

   /*
      caso não queira usar o decorator @Injectable para tornar o serviço injetável em qualquer
      lugar do projeto, tem 2 opções:

      1 eh criar esse array de providers colocando o service e outra eh colocar o service no array
      de providers no módulo principal da aplicação
   */

  // providers: [EventoService]
})
export class EventosComponent implements OnInit {

  // eventos: any  = []; // inicializo como array para ser possível usar o length

  titulo = 'Eventos';
  eventos: Evento[]; // inicializo como array para ser possível usar o length
  eventosFiltrados: Evento[];
  evento: Evento;

  modoSalvar = 'post';

  imagemLargura = 50;
  imagemMargem  = 2;
  mostrarImagem = false;
  modalRef: BsModalRef;

  registerForm: FormGroup;

  bodyDeletarEvento = '';

  file: File;
  fileNameToUpdate: string;

  dataAtual: string;

  _filtroLista = '';

  // private toastr: ToastrService => estava dentro do construtor

  constructor(private eventoService: EventoService, private modalService: BsModalService,
              private fb: FormBuilder, private localeService: BsLocaleService) {

                this.localeService.use('pt-br'); // faço isso para usar a data no formato português
              }

  // encapsulamento

  get filtroLista() {
    return this._filtroLista;
  }

  set filtroLista(value: string) {
    this._filtroLista     = value;
    this.eventosFiltrados = this._filtroLista ? this.filtrarEventos(this._filtroLista) : this.eventos;
  }

  editarEvento(evento: Evento, template: any) {
    this.modoSalvar = 'put';
    this.openModal(template);

    /*
      para evitar o problema de ao clicar em editar a imagem automaticamente desaparecer, devido
      ao binding q ocorre com o template, iremos copiar os dados de evento para this.evento e n
      apenas associar, ou seja, fazer um binding novamente através do sinal do igual

      a cópia eh feita através do Object.assign
    */

    // this.evento = evento;

    this.evento = Object.assign({}, evento);

    this.fileNameToUpdate = evento.urlImagem.toString(); // pego o nome da imagem

    // passando string vazia para urlImagem evitará um erro dizendo q n existe value pro input
    // eh necessário deixar nulo, pois o input do tipo file só eh get, n eh set, ou seja, n
    // consegue setar nenhum dado para urlImagem
    this.evento.urlImagem = '';

    // uso o patchValue para popular os campos do formulário que tem o nome registerForm
    this.registerForm.patchValue(this.evento);

  }

  novoEvento(template: any) {
    this.modoSalvar = 'post';
    this.openModal(template);
  }

  excluirEvento(evento: Evento, template: any) {
    this.openModal(template);
    this.evento = evento;
    this.bodyDeletarEvento = `Tem certeza que deseja excluir o Evento: ${evento.tema}, Código: ${evento.id}`;
  }

  confirmeDelete(template: any) {
    this.eventoService.deleteEvento(this.evento.id).subscribe(
      () => {
          template.hide();
          this.getEventos();
          // this.toastr.success('Deletado com sucesso');
        }, error => {
          // this.toastr.error(`Erro ao deletar: ${error}`);
          console.log(error);
        }
    );
  }

  openModal(template: any) {
    this.registerForm.reset(); // antes de abrir o modal, reseto o formulário, para limpar os campos
    template.show();
  }

  // openModal(template: TemplateRef<any>) {
  //   this.modalRef = this.modalService.show(template);
  // }

  // eventosFiltrados: any = [];

  // eventos: any = [
  //   {
  //     EventoId : 1,
  //     Tema     : 'Angular',
  //     Local    : 'Belo Horizonte'
  //   },

  //   {
  //     EventoId : 2,
  //     Tema     : '.Net Core',
  //     Local    : 'São Paulo'
  //   },

  //   {
  //     EventoId : 1,
  //     Tema     : '.Net Core e Angular',
  //     Local    : 'Rio de Janeiro'
  //   },
  // ];

  // constructor(private http: HttpClient) { }

  ngOnInit() {
    this.validation();
    this.getEventos();
  }

  filtrarEventos(filtrarPor: string): Evento[] {
    filtrarPor = filtrarPor.toLowerCase();

    // filtro os eventos vindos da API
    return this.eventos.filter(
      evento => evento.tema.toLowerCase().indexOf(filtrarPor) !== -1
    );
  }

  alternarImagem() {
    this.mostrarImagem = !this.mostrarImagem;
  }

  validation() {
    // this.registerForm = new FormGroup({

    this.registerForm = this.fb.group({


      /*
        registerForm eh o nome do formulário e aqui nele terá q colocar os campos do formulário

        o primeiro parâmetro do construtor de FormControl eh o q vc quer q seja carregado nos campos
        do formulário qnd este método for executado

        usando FormBuilder n eh necessário usar new FormControl(), deixa menos poluído o código
      */

      tema: ['', [Validators.required, Validators.minLength(4), Validators.maxLength(50)]],
      local: ['', Validators.required],
      dataEvento: ['', Validators.required],
      urlImagem: ['', Validators.required],

      // ao usar o max (estou dizendo q n quero q tenha mais de 120.000 pessoas)
      qtdPessoas: ['', [Validators.required, Validators.max(120000)]],

      telefone: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],

      // tema: new FormControl('', [
      //   Validators.required, Validators.minLength(4), Validators.maxLength(50)]),
      // local: new FormControl('', Validators.required),
      // dataEvento: new FormControl('', Validators.required),
      // urlImagem: new FormControl('', Validators.required),

      // // ao usar o max (estou dizendo q n quero q tenha mais de 120.000 pessoas)
      // qtdPessoas: new FormControl('', [
      //   Validators.required, Validators.max(120000)]),

      // telefone: new FormControl('', Validators.required),
      // email: new FormControl('', [
      //   Validators.required, Validators.email]),
    });
  }

  onFileChange(event) {

    const reader = new FileReader();

    // se a imagem existir e tiver comprimento
    if (event.target.files && event.target.files.length) {
      this.file = event.target.files;
    }
  }

  uploadImagem() {

    if (this.modoSalvar === 'post') {

      /*
        ao fazer this.evento.urlImagem eh armazenado na urlImagem através do formulário
        o seguinte caminho da imagem: c:\fakefolder\nome_imagem.jpg

        logo para pegar somente o nome da imagem usaremos split

        no split o caractere separador eh o \, e o segundo parâmetro eh 3, pois terei um
        array formado após o split de tamanho máximo 3: ['c', 'fakepath', 'nome_imagem.jpg']

        obs: o primeiro parâmetro eh um caractere especial
     */

      const nomeArquivo = this.evento.urlImagem.split('\\', 3);
      this.evento.urlImagem = nomeArquivo[2]; // pego o nome da imagem

      /*
        para a imagem ao ser alterada ser modificada automaticamente na nossa tabela, sem precisar
        de refresh manual, iremos acrescentar junto com o nome da imagem o tempo em milisegundos
        para diferenciar as imagens, evitando cache, esse tempo em milisegundos será passado na
        url da imagem
      */

      this.eventoService.postUpload(this.file, nomeArquivo[2]).subscribe(

        () => {
          this.dataAtual = new Date().getMilliseconds().toString();
          this.getEventos();
        }
      );

    } else {
      /*
        ao fazer isso ao alterar a imagem, a imagem anterior n continuará no servidor, ou seja,
        n continuará no folder Resources/Imagens, pois o nome da imagem atual (this.evento.urlImagem)
        será o mesmo nome da imagem anterior (this.fileNameToUpdate)
      */
      this.evento.urlImagem = this.fileNameToUpdate;
      this.eventoService.postUpload(this.file, this.fileNameToUpdate).subscribe(

        () => {
          this.dataAtual = new Date().getMilliseconds().toString();
          this.getEventos();
        }
      );
    }

  }

  salvarAlteracao(template: any) {

    if (this.registerForm.valid) {

      if (this.modoSalvar === 'post') {

        /*
          coloco {} (objeto vazio) para evitar a sobrescrita dos dados, outra forma seria
          inicializar o objeto com valores nulos

          o objeto vazio tem esse poder, pois ao colocar ele dentro de Object.assign eu tô
          dizendo q vai ser concatenado {} com this.registerForm.value
        */
        this.evento = Object.assign({}, this.registerForm.value);

        this.uploadImagem();

        // como é um Observable para funcionar precisa do subscribe
        this.eventoService.postEvento(this.evento).subscribe(

          // novoEvento armazena o q está armazenado em this.evento
          // parâmetro em caso de sucesso
          (novoEvento: Evento) => {
            console.log(novoEvento);
            template.hide();
            this.getEventos();
            // this.toastr.success('Inserido com sucesso');
          },
          error => {
            // this.toastr.error(`Erro ao tentar inserir: ${error}`);
            console.log(error); // parâmetro em caso de erro
          }
        );
      } else {

        /*
          na atualização é preciso passar junto com os dados do formulário (this.registerForm.value)
          o id do evento que vc está editando (id: this.evento.id), pois assim o bd terá a certeza
          de qual evento atualizar
        */

        this.evento = Object.assign({id: this.evento.id}, this.registerForm.value);

        this.uploadImagem();

        this.eventoService.putEvento(this.evento).subscribe(

          () => {
            template.hide();
            this.getEventos();
            // this.toastr.success('Editado com sucesso');
          },
          error => {
            // this.toastr.error(`Erro ao tentar editar: ${error}`);
            console.log(error);
          }
        );
      }
    }
  }

  getEventos() {

    // this.http.get('http://localhost:5000/api/values').subscribe(response => {

    /*
      coloco a dataAtual aqui, pois qnd ocorre a mudança de imagem por causa do chache e pq
      as imagens anteriores trocadas pelas atuais possuem o msm nome, fazem com q a nova imagem
      n apareça na tabela, por isso passo a dataAtual em milisegundos q será passada na url da
      imagem, com isso o nome da imagem fica diferente para o browser evitando cache
    */

    this.dataAtual = new Date().getMilliseconds().toString();

    this.eventoService.getAllEvento().subscribe(

      (events: Evento[]) => {

      this.eventos          = events;
      this.eventosFiltrados = events;
      console.log(this.eventos);

    },
      error => {
        // this.toastr.error(`Erro ao buscar eventos: ${error}`);
        console.log(error);
      }
    );
  }

}
