import {Lote} from './Lote';
import {RedeSocial} from './RedeSocial';
import {Palestrante} from './Palestrante';

// export interface Evento {
export class Evento {

    constructor() {} // como Evento eh uma classe agora, coloco o construtor

    id: number;
    local: string;
    dataEvento: Date;
    tema: string;
    qtdPessoas: number;
    urlImagem: string;
    telefone: string;
    email: string;

    lotes: Lote[];

    redesSociais: RedeSocial[];

    palestrantesEventos: Palestrante[];
}
