// namespace ProAgil.API.Models => antes ficava em webapi e no diretório models

using System; // necessário importar por causa do DateTime
using System.Collections.Generic;

namespace ProAgil.Domain

{
    public class Evento
    {
        /*
            se o atributo for chamado Id ou o nome Id estiver no final do atributo e for do
            tipo int, este atributo será convertido para coluna do tipo primary key e auto
            increment
        */

        public int Id {get; set;}
        public string Local {get; set;}
        public DateTime DataEvento {get; set;}
        public string Tema {get; set;}
        public int QtdPessoas {get; set;}
        public string UrlImagem {get; set;}
        public string Telefone {get; set;}
        public string Email {get; set;}

        public List<Lote> Lotes {get; set;}

        public List<RedeSocial> RedesSociais {get; set;}

        public List<PalestranteEvento> PalestrantesEventos {get; set;}

        // public int EventoId {get; set;}
        // public string Local {get; set;}
        // public string DataEvento {get; set;}
        // public string Tema {get; set;}
        // public int QtdPessoas {get; set;}
        // public string Lote {get; set;}
        // public string UrlImagem {get; set;}
    }
}