namespace ProAgil.Domain
{
    public class PalestranteEvento // multiplicidade => n por n
    {
        public int PalestranteId {get; set;}
        public int EventoId {get; set;}
        
        public Palestrante Palestrante {get; set;}
        public Evento Evento {get; set;}
    }
}