using System;

namespace ProAgil.Domain
{
    public class Lote
    {
        public int Id {get; set;}
        public string Nome {get; set;}
        public decimal Preco {get; set;}
        public DateTime? DataInicio {get; set;}
        public DateTime? DataFim {get; set;}
        public int Quantidade {get; set;}

        /*
            EventoId => Evento (Entidade) e Id (id)
            EventoId => chave estrangeira
        */

        public int EventoId {get; set;}

        
        /*
            eh necessário deixar só o get nas entidades relacionadas, pois elas servirão somente
            de leitura em Lote, ou seja, em Lote eu n poderei setá-las, criá-las, pq senão
            ocorrerá um loop infinito, por exemplo:

            Lote tem evento, evento tem lista de lotes, lote tem evento, 
            evento tem lista de lotes, e assim o fluxo vai se repetindo
        */

        public Evento Evento {get;}

        //public Evento Evento {get; set;}
    }
}