using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNetCore.Identity;

namespace ProAgil.Domain.Identity
{
    /*
        toda entidade Identity espera um tipo de chave, se n passarmos nd automaticamente a chave
        primária será uma hash, por isso passamos o tipo int
    */
    public class User : IdentityUser<int>
    {
        /*
            User sendo do tipo IdentityUser já usa os seus atributos
            IdentityUser n tem esse campo abaixo e quero colocá-lo na tabela
        */
        [Column(TypeName="nvarchar(150)")]
        public string FullName {get; set;}

        public List<UserRole> UserRoles {get; set;}
    }
}