using System.Collections.Generic;
using Microsoft.AspNetCore.Identity;

namespace ProAgil.Domain.Identity
{
    public class Role : IdentityRole<int>
    {
        /*
            Role e User se relacionaram através da multiplicidade n por n, por isso coloco
            uma lista de UserRole
        */

        public List<UserRole> UserRoles {get; set;}
    }
}